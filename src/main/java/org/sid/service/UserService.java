package org.sid.service;

import java.util.List;

import org.sid.dao.IUserRepository;
import org.sid.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class UserService implements IUserService
{
	@Autowired
	private IUserRepository dao;

	@Override
	public void addUser(User user)
	{
		dao.save(user);

	}

	@Override
	public void deleteUser(Long idUser)
	{
		dao.delete(idUser);

	}

	@Override
	public User getUserById(Long idUser)
	{
		return dao.findOne(idUser);
	}

	@Override
	public List<User> findUsersByFirstName(String firstName)
	{
		return dao.fingUsersByFirstName(firstName);
	}

	@Override
	public List<User> getAllUsers()
	{
		return dao.findAll();
	}

}
