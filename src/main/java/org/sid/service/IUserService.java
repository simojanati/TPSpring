package org.sid.service;

import java.util.List;

import org.sid.model.User;


public interface IUserService
{

	public void addUser(User user);

	public void deleteUser(Long idUser);

	public User getUserById(Long idUser);

	public List<User> findUsersByFirstName(String firstName);

	public List<User> getAllUsers();
}
