package org.sid.dao;

import java.util.List;

import org.sid.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface IUserRepository extends JpaRepository<User, Long>
{
	@Query("select u from User u where u.firstName=:firstName")
	public List<User> fingUsersByFirstName(@Param("firstName") String firstName);

}
