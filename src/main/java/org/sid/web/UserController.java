package org.sid.web;

import java.util.List;

import javax.websocket.server.PathParam;

import org.sid.model.User;
import org.sid.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class UserController
{
	@Autowired
	private IUserService metier;

	@RequestMapping("/users")
	public ModelAndView users()
	{
		ModelAndView modelAndView = new ModelAndView("users");
		getAllUsers(modelAndView);
		return modelAndView;
	}

	@RequestMapping("/addUser")
	public String addUser(String firstName, String lastName, String phoneNumber, ModelAndView modelAndView)
	{
		metier.addUser(new User(firstName, lastName, phoneNumber));
		getAllUsers(modelAndView);
		return "users";
	}

	@RequestMapping(name = "/deleteUser",method = RequestMethod.GET)
	public String deleteUser(Long idUser, ModelAndView modelAndView)
	{
		metier.deleteUser(idUser);
		getAllUsers(modelAndView);
		return "users";
	}

	private List<User> getAllUsers(ModelAndView modelAndView)
	{
		List<User> users = metier.getAllUsers();
		modelAndView.addObject("users", users);
		return users;

	}
	
	@RequestMapping("/test/user/{name}")
	public List<User> users(@PathVariable String name){
		return metier.findUsersByFirstName(name);
	}
	




}
