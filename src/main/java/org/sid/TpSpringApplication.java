package org.sid;

import org.sid.dao.IUserRepository;
import org.sid.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSpringApplication implements CommandLineRunner {

	@Autowired
	private IUserRepository userRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(TpSpringApplication.class, args);
		
	}

	@Override
	public void run(String... arg0) throws Exception
	{
		/*userRepository.save(new User("test1", "test1", "0611223344"));
		userRepository.save(new User("test2", "test2", "0611223344"));
		userRepository.save(new User("test3", "test3", "0611223344"));
		*/
	}
	
	
}
